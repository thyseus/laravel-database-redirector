<?php

namespace Thyseus\DBRedirector\Providers;

/**
 * Class RedirectorServiceProvider
 *
 * @package Thyseus\Providers\RedirectorServiceProvider
 */
class RedirectorServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public const NAMESPACE = 'DBRedirector';

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->loadFactoriesFrom(__DIR__ . '/../Database/Factories');

        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', self::NAMESPACE);

        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path('vendor/db-redirector'),
        ], 'public');
    }

    public function register()
    {

    }
}