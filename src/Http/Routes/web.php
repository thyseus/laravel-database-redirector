<?php

use Illuminate\Support\Facades\Route;
use Thyseus\DBRedirector\Controllers\RedirectController;

Route::prefix('admin')
    ->middleware([
        'web',
        'admin',
    ])
    ->group(function () {
        Route::resource('redirects', RedirectController::class)
            ->except('show');
    });


